var express = require('express');
var mongoose = require('mongoose');

 var db = mongoose.connection;

 db.on('error', console.error);
 db.once('open', function() {

 });

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://ctac:ctac@ds147537.mlab.com:47537/events_db');
//mongoose.connect('mongodb://localhost:27017/events_db');
/*mongoose.connect('mongodb://localhost/geospatial_db', function(err) {
    if (err) throw err;

    // do something...
});*/

var event = new mongoose.Schema({
    name: String
    , place: String
    , price: Number
    , date: Date
    , loc: {
        type: [Number],  // [<longitude>, <latitude>]
        index: '2d'      // create the geospatial index
    }
    , tags: String
    , deskr: String
    , image: String
    , moderated: Boolean
});

var user = new mongoose.Schema({
    name: {
        type : String,
        required: true
    }
    , organization: String
    , email: {
        type : String,
        required: true,
        unique: true
    }
    , role: String
    , password:{
        type: String,
        required: true
    }

});

var Event = mongoose.model('Event', event);
var User = mongoose.model('User', user);

module.exports.Event = Event;
module.exports.User = User;