var express = require('express');
var router = express.Router();
var Event = require('../dbase.js').Event;
var User = require('../dbase.js').User;

var NodeGeocoder = require('node-geocoder');

var options = {
  provider: 'google',

  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyAz46THD37Ca33epljAk9j3399ISdgbKOU', // for Mapquest, OpenCage, Google Premier
  formatter: 'string', // 'gpx', 'string', ...
  formatterPattern: '%lo %la',
};

var geocoder = NodeGeocoder(options);

/* GET users listing. */
router.get('/getall', function(req, res, next) {
  var evsArr;
  Event.find( function (err, evs) {
    if (err) console.error(err.stack||err);
    evsArr = evs.map(function(d){ return d.toObject() });
    res.send(JSON.stringify(evsArr));
    res.end();
  });
});

router.get('/getevent', function(req, res) {
  console.log(req.query.name);
  Event.findOne({name: req.query.name }, function (err, event) {
    if (err) console.error(err.stack||err);
    res.send(JSON.stringify(event));
    res.end();
  });
});

router.post('/addevent', function(req, res){

  var geo = {};
  geocoder.geocode(req.body.place, function(err, res) {
    var res1 = res.toString().split(' ');
    geo.lng = res1[0];
    geo.lat = res1[1];
    //var evsIm = req.files.evImage;
    //var base64String = evsIm.data.toString('base64');
    var event = new Event({
      name: req.body.name,
      city: req.body.city,
      place: req.body.place,
      price: req.body.price,
      date: req.body.date,
      loc: [geo.lng, geo.lat],
      //img: base64String
    });
    console.log(event);

    event.save(function(err, event) {
      if (err) return console.error(err);
      console.dir("Success");
    });
  });
  res.redirect('/');
})


router.put('/putevent', function(req, res){

  Event.findOne({name: req.query.name }, function (err, event) {
    if (err) console.error(err.stack||err);

    var geo = {};
    geocoder.geocode(req.body.place, function(err, res) {
      console.log("geo before: " + res);
      var res1 = res.toString().split(' ');
      geo.lng = res1[0];
      geo.lat = res1[1];
      //var evsIm = req.files.evImage;
      //var base64String = evsIm.data.toString('base64');
      event.name = req.body.name;
      event.city = req.body.city;
      event.place = req.body.place;
      event.price = req.body.price;
      event.date = req.body.date;
      event.loc = [geo.lng, geo.lat];
        //img: base64String
      event.save(function(err, event) {
        if (err) return console.error(err);
        console.dir("Success");
        res.send(JSON.stringify(event));
        res.end();
      });
    });
  });
});
module.exports = router;
