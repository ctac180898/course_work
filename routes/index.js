var express = require('express');
const crypto = require('crypto');
const session = require('express-session');
const passport = require('passport');
var router = express.Router();
var Event = require('../dbase.js').Event;
var User = require('../dbase.js').User;
var fs = require('fs');
var NodeGeocoder = require('node-geocoder');

var formatter = {
    format: function(data) {
        var strings = [];
        strings.push(data[0].longitude);
        strings.push(data[0].latitude);
        return strings;
    },
}

var options = {
    provider: 'google',

    httpAdapter: 'https', // Default
    apiKey: 'AIzaSyBMmBOKthuQIP6qy9yNqffb-9p6nVvvCl0', // for Mapquest, OpenCage, Google Premier
    formatter: formatter

};

var geocoder = NodeGeocoder(options);
const sessionSecret = '$EQRET@№GENT';
const serverSalt = "do!hURTS@lT";

function hash(pass, em) {
    return crypto.createHash('md5').update(em + pass + serverSalt).digest("hex");
}

router.get('/', function(req, res, next) {
    var user = req.session.user;
    res.render('index', { user: user , message1: 'no'});
});

router.post('/signup', function(req, res) {
    User.findOne({email: req.body.email}, function(err, user){
        if (err) {
            res.redirect('/');
            return console.error(err);
        }
    })
    var user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hash(req.body.password),
        organization: req.body.orgName,
        role: 'user'
    });
    user.save(function(err, user) {
        if (err) return console.error(err);
        console.dir("Success");
    });

    res.redirect('/');
});

router.post('/signin', function(req, res) {

    User.findOne({email: req.body.email, password: hash(req.body.password)}, function(err, user) {
        if (err) {
            console.log("error");
            res.redirect('/');
            return console.error(err);
        }

        else if(user != null) {
            console.log("found");
            console.dir(user.email);
            req.session.user = { name: user.name, organization: user.organization, email: user.email, role: user.role };
            res.redirect('/');
        }
        else{
            console.log("don't exist");
            res.render('index', { user: user , message1: 'nouser'});
        }
    });

});

router.get('/logout', function(req, res, next) {
    delete req.session.user;
    res.redirect('/');
});

router.post('/addevent', function(req, res){

    var geo = {};
    geocoder.geocode(req.body.place, function(err, res) {
        console.log("res: " + res.toString());
        var res1 = res.toString().split(',');
        geo.lng = res1[0];
        geo.lat = res1[1];
        console.log("geo: " + res1);
        var evsIm = req.files.evImage;
        var base64String = evsIm.data.toString('base64');
        var event = new Event({
            name: req.body.name,
            city: req.body.city,
            place: req.body.place,
            price: req.body.price,
            date: req.body.date,
            loc: [geo.lng, geo.lat],
            image: base64String,
            moderated: false
        });
        console.log(event);

        event.save(function(err, event) {
            if (err) return console.error(err);
            console.dir("Success");
        });
    });
    res.redirect('/');
})

router.get('/getnearbyevs',function (req, res, next) {
    var limit = 100;
    var maxDistance =  1000;
    maxDistance /= 63.71;
    maxDistance /= 1.3;
    console.log("maxDistance: " + maxDistance);

    var coords = [];
    coords[0] = req.query.lng;
    coords[1] = req.query.lat;

    console.log("coords: " + coords)

    Event.find({
        loc: {
            $near: coords,
            $maxDistance: maxDistance
        }
    }).limit(limit).exec(function(err, locations) {
        if (err) {
            return res.json(500, err);
        }
        res.status(200).json(locations);
    });
})

router.get('/getallevs', function(req, res) {
    var evsArr;
    Event.find({moderated: true}, function (err, evs) {
        if (err) console.error(err.stack||err);
        evsArr = evs.map(function(d){ return d.toObject() });
        res.send(JSON.stringify(evsArr));
        res.end();
    });
});

router.get('/getevent', function(req, res) {
    Event.findOne({name: req.query.name, moderated: true}, function (err, event) {
        if (err) console.error(err.stack||err);
        res.send(JSON.stringify(event));
        res.end();
    });
});


router.get('/moderate', function(req, res) {
    console.log(req.query.page);
    console.log(req.session.user.role);
    if(req.session.user.role == 'admin') {

        Event.find(function (err, events) {
            if (err) console.error(err.stack || err);
            if (events.length < 6) {
                console.log(events);
                res.render('moderate', {events: events});
            }
            else {
                console.log(req.session.user.role);
                console.log(events);
                var page = req.query.page;
                var startPos = page * 5 - 5;
                var lastPos = startPos + 5;
                var evarr = events.slice(startPos, lastPos);
                res.render('moderate', {events: evarr});
            }
        });
    }
    else{
        res.render('permden');
    }
});

router.post('/moderate', function(req, res){
    if(req.session.user.role == 'admin'){
        console.log("body: " + req.body.event0);
        res.redirect('/moderate?page=1');
    }
    else{
        res.render('permden');
    }
})

router.get('/getsortedevents', function(req, res){
    var evsArr;
    console.log("getsortedevents");
    var datefrom = new Date();
    var dateto = new Date();
    var pricefrom = 0;
    var priceto = Infinity;
    dateto.setFullYear(dateto.getFullYear() + 5);
    if(req.query.fdatefrom != ''){
        var datearr = req.query.fdatefrom.split('-');
        datefrom.setFullYear(datearr[0], datearr[1]-1, datearr[2])
    }
    if(req.query.fdateto != ''){
        var datearr = req.query.fdateto.split('-');
        dateto.setFullYear(datearr[0], datearr[1]-1, datearr[2])
    }
    if(req.query.fpricefrom != ''){
        pricelow = req.query.fpricefrom;
    }
    if(req.query.fpriceto != ''){
        pricelow = req.query.fpriceto;
    }
    Event.find({date: {$gt: datefrom, $lt: dateto}, price:{$gt: pricefrom, $lt: priceto}}, function(err, evs){
        evsArr = evs.map(function(d){ return d.toObject() });
        res.send(JSON.stringify(evsArr));
        console.log(evsArr);
        res.end();
    })
})

module.exports = router;
